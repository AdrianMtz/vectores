
package vectores;
import java.awt.Color;
 import java.util.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
public class Vectores {
    public static void main(String[] args) {
        try{
            int N = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresa el tamaño del arreglo: "));
            int a[] = new int[N];
            for(int i=0;i<N;i++){
                a[i] = Integer.parseInt(JOptionPane.showInputDialog(null, "El arreglo estatico es de " + N + " ingresa el numero en la posicion: " + i));
            }
            grafico gra=new grafico(a,N);
            JFrame ventana= new JFrame();
            ventana.setSize(800,200);
            ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            ventana.setBackground(Color.GREEN);
            ventana.add(gra);
            ventana.setVisible(true); 
        }catch(NumberFormatException ex){
            JOptionPane.showMessageDialog(null, "No se ha introducido un numero!");
            JOptionPane.showMessageDialog(null, "El programa se cerrará");
            System.exit(0);
        }
    }
}
