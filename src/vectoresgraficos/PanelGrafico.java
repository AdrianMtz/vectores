package vectoresgraficos;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;
public class PanelGrafico extends JPanel{
    private final ArrayList<Integer> datos;
    
    public PanelGrafico(ArrayList<Integer> datos) {
        this.datos = datos;
    }
    
    @Override
    public void paintComponent(Graphics g){
        int x = 5, y = 50;
        for(int i=0;i<datos.size();i++){
            g.drawRect(x, y, 50, 25);
            g.drawString(datos.get(i) + "", x + 20, y + 13);
            x+=55;
        }
    }
    
}
