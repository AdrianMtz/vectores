package vectoresgraficos;
import java.awt.Color;
import java.util.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
public class VectoresGraficos {
    public static void main(String[] args) {
        
        int opcion;
        ArrayList<Integer> datos = new ArrayList();
        while((opcion = Integer.parseInt(JOptionPane.showInputDialog(null, "¿Desea agregar un elemento? (1 - SI, 2 - NO)"))) != 2){
            datos.add(Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce un numero: ")));
        }
        JFrame f = new JFrame("Vectores Graficos");
        PanelGrafico panel = new PanelGrafico(datos);
        f.setLocation(0, 0);
        f.setSize(800, 200);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         f.setBackground(Color.RED);
        f.add(panel);
        f.setVisible(true);
    }
    
}
